<?php
namespace Container;

use Container\Exception\ConfigurationException;

/**
 * Класс Container реализует примитивный IoC-контейнер
 *
 * @package Container
 */
class Container implements ContainerInterface
{
    protected $config = [];
    protected $loaded = [];

    /**
     * Конструкьтор
     *
     * Принимает на входе конфигурационный файл (синтаксис php.ini).
     * Каждая секция этого файла - настройки отдельного сервиса.
     * [<название сервиса>]
     * class = <Класс сервиса>
     * params[] = <параметр конструтора класса 1>
     * params[] = <параметр конструтора класса 2>
     * ...
     *
     * Например:
     * [pi.service]
     * class = Pi\Pi
     * params[] = @container
     * params[] = 4
     * params[] = 5000000
     * params[] = 9999999
     *
     * При вызове $container->get("pi.service") будет возвращен new Pi\Pi(@container, 4, 5000000, 9999999)
     *
     * Параметр %service_name заменится на название сервиса.
     * Параметр %config_name заменится на путь до конфигурационного файла.
     *
     * Параметр, начинающийся с символа @ - ссылка на другой сервис.
     * Например:
     * params = @worker.service
     *
     * По названию @container возвращается сам контейнер.
     *
     * Специальная секция [container] хранит настройки самого контейнера.
     * В этой секции можно указать параметр "include", содержащий путь до файла конфигурации, который надо включить в контейнер
     *
     * Например:
     * [container]
     * include = Parallel/parallel.conf
     *
     * @param string $file Путь до конфигурационного файла (может быть маской)
     */
    public function __construct($file)
    {
        $this->load($file);

        if (isset($this->config['container']) && isset($this->config['container']['include'])) {
            foreach ((array)$this->config['container']['include'] as $file) {
                $this->load($file);
            }
        }
    }

    /**
     * Вернуть новый объект класса Container
     *
     * @param string $file Путь до конфигурационного файла
     * @return Container
     */
    public static function create($file)
    {
        return new static($file);
    }

    /**
     * Загрузить файлы конфигруации по маске
     * @param $files
     */
    protected function load($files)
    {
        foreach (glob($files) as $file) {
            if (!in_array($file, $this->loaded)) {
                $this->loaded[] = $file;
                $config = parse_ini_file($file, true);

                foreach($config as $service => $params) {
                    $config[$service]['service_name'] = $service;
                    $config[$service]['config_name'] = $file;
                }

                $this->config = array_merge($config, $this->config);
            }
        }
    }

    /**
     * Вернуть новый экземпляр сервиса
     *
     * @param string $name Название сервиса
     * @return object Экземпляр сервиса
     *
     * @throws ConfigurationException Когда сервис не найден
     * @throws ConfigurationException Когда сервису не назначен класс
     * @throws ConfigurationException Когда сервису назначен несуществующий класс
     */
    public function get($name)
    {
        if ($name === 'container') {
            return $this;
        }

        if (!isset($this->config[$name])) {
            throw new ConfigurationException("Service $name not found");
        }

        if (!isset($this->config[$name]['class'])) {
            throw new ConfigurationException("Service $name has no class");
        }

        $class = $this->config[$name]['class'];

        if (!class_exists($class)) {
            throw new ConfigurationException("Service $name has the nonexistent class $class");
        }

        $reflect = new \ReflectionClass($class);

        $params = isset($this->config[$name]['params']) ? (array)$this->config[$name]['params'] : [];
        $params = $this->inject($params, $this->config[$name]);

        return $reflect->newInstanceArgs($params);
    }

    /**
     * Внедрить в параметры вместо ссылок экземпляры сервисов и параметры
     *
     * @param array $params Массив параметров
     * @param array $config Конфигурация сервиса
     * @return array Обработанный массив параметров
     *
     * @throws ConfigurationException Когда внедряемый сервис не найден
     * @throws ConfigurationException Когда внедряемому сервису не назначен класс
     * @throws ConfigurationException Когда внедряемому сервису назначен несуществующий класс
     */
    protected function inject(array $params, array $config)
    {
        foreach ($params as $key => $param) {
            if (is_string($param) && strlen($param) > 1) {
                if($param[0] === '@') {
                    $params[$key] = $this->get(substr($param, 1));
                }

                if($param[0] === '%') {
                    $name = substr($param, 1);

                    $params[$key] =  isset($config[$name]) ? $config[$name] : null;
                }
            }
        }

        return $params;
    }
}