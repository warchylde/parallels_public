<?php
namespace Container;

use Container\Exception\ConfigurationException;

interface ContainerInterface
{
    /**
     * Вернуть новый экземпляр сервиса
     *
     * @param string $name Название сервиса
     * @return object Экземпляр сервиса
     *
     * @throws ConfigurationException Когда сервис не найден
     * @throws ConfigurationException Когда сервису не назначен класс
     * @throws ConfigurationException Когда сервису назначен несуществующий класс
     */
    public function get($name);
}