<?php
namespace Container\Exception;

/**
 * Класс исключений для ошибок конфигурации контейнера
 *
 * @package Container
 */
class ConfigurationException extends \Exception implements ExceptionInterface
{

}