<?php
namespace Random;

/**
 * Класс Random реализует работу с генератором случаных чисел
 *
 * @package Random
 */
class Random
{
    protected $min;
    protected $max;

    /**
     * Конструтор
     *
     * @param int $min Дефолтовое минимально генерируемое число
     * @param int $max Дефолтовое максимальное генерируемое число
     */
    public function __construct($min = 0, $max = 1)
    {
        $this->min = $min;
        $this->max = $max;

        $this->seed();
    }

    /**
     * Произвести посев генератора случайных чисел
     */
    public function seed()
    {
        mt_srand($this->getSeed());
    }

    /**
     * Дать посевной материал
     *
     * @return float
     */
    protected function getSeed()
    {
        list($usec, $sec) = explode(' ', microtime());

        return (float)$sec + ((float)$usec * 100000);
    }

    /**
     * Сгенерировть случаной число в диапазоне от минимума до максимума, указаного или в конструторе или при
     * вызове этой функции.
     *
     * @param null|int $min
     * @param null|int $max
     * @return int
     */
    public function get($min = null, $max = null)
    {
        if ($min === null) {
            $min = $this->min;
        }

        if ($max === null) {
            $max = $this->max;
        }

        return $min + mt_rand() / mt_getrandmax() * ($max - $min);
    }
}