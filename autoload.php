<?php
define('NAMESPACE_SEPARATOR', '\\');

spl_autoload_register(
    function ($className) {
        $className = ltrim($className, NAMESPACE_SEPARATOR);
        $fileName = '';

        if ($lastNsPos = strrpos($className, NAMESPACE_SEPARATOR)) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace(NAMESPACE_SEPARATOR, DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR;
        }

        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className).'.php';

        require __DIR__.DIRECTORY_SEPARATOR.$fileName;
    }
);
