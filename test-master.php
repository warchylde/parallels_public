<?php
require 'autoload.php';

use Parallel\Worker;
use Parallel\Pool;
use Parallel\Pipes;
use Parallel\Transceiver;

$num = 4;
$pool = new Pool();

for ($i = 0; $i < $num; $i++) {
    $worker = Worker::create(new Pipes(new Transceiver()))
        ->setCommand(PHP_BINARY.' test-child.php')
        ->addListener('test', function () {
            return time();
        })
    ;

    $pool->insert($worker);
}

$pool->run(function (Worker $worker, $index) {
    if (!mt_rand(0, 5)) {
        echo "$index: {$worker->getNum()}\n";
    }
});
