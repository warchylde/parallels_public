<?php
namespace Pi;

use Container\ContainerInterface;
use Parallel\Pool;
use Parallel\Transceiver;
use Parallel\Worker;
use Parallel\WorkerInterface;
use Random\Random;

/**
 * Класс Pi реализует параллельне вычисление числа pi методом Монте-Карло
 *
 * @package Pi
 */
class Pi
{
    protected $container;
    protected $number = 4;
    protected $minIterations;
    protected $maxIterations;

    protected $iterations;
    protected $data = [];

    /**
     * Конструктор.
     *
     * @param ContainerInterface $container Контейнер
     * @param int $numberOfProcesses Количество параллельных прооцессов
     * @param int $minIterations Диапазон количества итераций (минимум)
     * @param int $maxIterations Диапазон количества итераций (максимум)
     */
    public function __construct(ContainerInterface $container, $numberOfProcesses, $minIterations, $maxIterations)
    {
        $this->container = $container;
        $this->number = $numberOfProcesses;
        $this->minIterations = $minIterations;
        $this->maxIterations = $maxIterations;
    }

    /**
     * Вернуть новый экземпляр класса Pi
     *
     * @param ContainerInterface $container Контейнер
     * @param int $numberOfProcesses Количество параллельных прооцессов
     * @param int $minIterations Диапазон количества итераций (минимум)
     * @param int $maxIterations Диапазон количества итераций (максимум)
     * @return self
     */
    public static function create(ContainerInterface $container, $numberOfProcesses, $minIterations, $maxIterations)
    {
        return new static($container, $numberOfProcesses, $minIterations, $maxIterations);
    }

    /**
     * Рассчитать число pi методом Монте Карло использую параллельные вычисления
     *
     * @param callable $callback Функция, которая будет вызываться случаныйм образом в процессе расчета, с параметром $this
     */
    public function calculate(callable $callback)
    {
        $pool = $this->container->get('pool.service');

        for($i = 0; $i < $this->number; $i++) {
            $worker = $this->container->get('pi.calculator.service')
                ->setOption('iterations', mt_rand($this->minIterations, $this->maxIterations))
                ->getWorker()
            ;

            $pool->insert($worker);
        }

        $pool->run(function() use($pool, $callback) {
            if(!mt_rand(0, 50)) {
                foreach($pool->getWorkers() as $index => $worker) {
                    if($worker->isRunning()) {
                        $this->setData($index, $worker->getData());
                    }
                }

                if(is_callable($callback)) {
                    call_user_func($callback, $this);
                }
            }
        });
    }

    /**
     * Сохранить рассчеты параллельного процесса
     *
     * @param int $index Индекс процесса
     * @param array $data Рассчетные данные
     */
    protected function setData($index, $data)
    {
        $this->data[$index] = $data;
    }

    /**
     * Рассчитать число pi
     * @return float
     */
    public function get()
    {
        return (array_sum(array_column($this->data, 'count')) * 4) / array_sum(array_column($this->data, 'iterations'));
    }
}