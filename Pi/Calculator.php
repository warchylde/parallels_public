<?php
namespace Pi;

use Parallel\Executable;
use Random\Random;

/**
 * Класс Calculator считает в отдельном процессе попадание в площадь радиуса
 *
 * @package Pi
 */
class Calculator extends Executable
{
    protected $radius;
    protected $iterations;

    /** @var Random */
    protected $random;
    protected $radiusSquare;
    protected $count = 0;

    /**
     * Приготовится к рассчету
     */
    protected function prepare()
    {
        $this->addListener('getData', [$this, 'getDataListener']);

        $radius = $this->getOption('radius', 1);
        $this->iterations = $this->getOption('iterations', 1e6);

        $this->random = new Random(0, $radius);
        $this->radiusSquare = $radius * $radius;
    }

    /**
     * Провести итерацию рассчета
     *
     * @param int $iteration Номер итерации
     * @return bool Если false - рассчет окончен
     */
    protected function iterate($iteration)
    {
        $x = $this->random->get();
        $y = $this->random->get();
        $inside = ($x * $x) + ($y * $y);

        if ($inside <= $this->radiusSquare) {
            $this->count++;
        }

        return $iteration < $this->iterations;
    }

    /**
     * Ответить на входящий запрос getData
     *
     * @return array Рассчетные данные: поле count - количество попаданий в площадь,
     *                                  поле iterations - всего итераций, которые произведет процесс
     */
    public function getDataListener()
    {
        return [
            'count' => $this->count,
            'iterations' => $this->iterations,
        ];
    }
}