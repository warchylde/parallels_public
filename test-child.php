<?php
require 'autoload.php';

use Parallel\Transceiver;

$transceiver = Transceiver::create()
    ->setStreams(fopen('php://stdout', 'w'), fopen('php://stdin', 'r'))
    ->addListener('getNum', function () {
        return rand(100000, 999999);
    })
;

$asked = false;

for ($i = 0; $i < 60; $i++) {
    $transceiver->listen();

    if (!$asked && !mt_rand(0, 5)) {
        $asked = true;

        file_put_contents('/tmp/parallel.'.posix_getpid(), $transceiver->execute('test'));
    }

    usleep(5e5);
}