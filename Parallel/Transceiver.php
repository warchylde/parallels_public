<?php
namespace Parallel;

use Parallel\Exception\ExecuteException;
use Parallel\Exception\LogicException;
use Parallel\Exception\RuntimeException;
use Parallel\Proto\Error;
use Parallel\Proto\Request;
use Parallel\Proto\Response;

/**
 * Класс Transceiver принимает/отправляет сообщения через потоки ввода/вывода
 * @package Parallel
 * @api
 */
class Transceiver implements TransceiverInterface
{
    const CHUNK_SIZE = 16384;
    const RESPONSE_DELAY = 50000;
    const RESPONSE_TIMEOUT = 5000000;

    protected $output;
    protected $input;
    protected $listeners = [];

    /**
     * Конструктор.
     */
    public function __construct()
    {
    }

    /**
     * Возвращает новый объект Transceiver
     *
     * @return Transceiver
     */
    public static function create()
    {
        return new static();
    }

    /**
     * Настраивает потоки ввода/вывода
     *
     * @param resource|string $output Поток вывода, открытый в режиме записи
     * @param resource|string $input Поток ввода, открытый в режиме чтения
     * @return self
     *
     * @throws RuntimeException Когда поток не поток или он открыт в несоответствующем режиме
     */
    public function setStreams($output, $input)
    {
        $this->output = $this->checkStream($output, 'w');
        $this->input = $this->checkStream($input, 'r');

        return $this;
    }

    /**
     * Проверить что передан именно поток и он открыт он в соответствущем режиме.
     * Если в качестве потока передана строка, метод попытается открыть ее как поток в соответствующем режиме.
     *
     * @param resource|string $stream Поток для проверки
     * @param string $mode Режим доступа
     * @return resource Проверенный поток
     *
     * @throws RuntimeException Когда передан не поток или он открыт в несоответствующем режиме
     */
    protected function checkStream($stream, $mode)
    {
        if (is_string($stream)) {
            $stream = fopen($stream, $mode);
        }

        if (!$this->isStream($stream, $mode)) {
            throw new RuntimeException('Given parameter is not a stream');
        }

        return $stream;
    }

    /**
     * Проверить что это поток в нужном режиме доступа.
     *
     * @param resource $stream Поток
     * @param string $mode Режим доступа
     * @return bool true - если поток, иначе false
     */
    protected function isStream($stream, $mode)
    {
        return is_resource($stream) && get_resource_type($stream) === 'stream' && stream_get_meta_data($stream)['mode'] === $mode;
    }

    /**
     * Читать из потока ввода строку
     *
     * @param int $timeout Таймаут ожидания данных в микросекундах
     * @return null|string Возвращает null или прочитанную строку
     *
     * @throws RuntimeException Когда функция stream_select не сработала
     * @throws RuntimeException Когда функция stream_get_line не сработала
     */
    protected function read($timeout = 0)
    {
        $str = null;

        $read = [$this->input];
        $write = null;
        $except = null;

        $res = stream_select($read, $write, $except, 0, $timeout);

        if (false === $res) {
            throw new RuntimeException('Can\'t stream select');
        }

        if ($res && !feof($this->input)) {
            $str = stream_get_line($this->input, self::CHUNK_SIZE, PHP_EOL);

            if (false === $str && !feof($this->input)) {
                throw new RuntimeException('Can\'t read from stream');
            }
        }

        return $str;
    }

    /**
     * Записать строку в поток вывода
     *
     * @param string $str Записываемая строка
     * @return self
     */
    protected function write($str)
    {

        fwrite($this->output, (string) $str . PHP_EOL);

        return $this;
    }

    /**
     * Слушать поток ввода на наличие входящего вызова и отвечать на него
     *
     * @return self
     *
     * @throws RuntimeException Когда функция stream_select не сработала
     * @throws RuntimeException Когда функция stream_get_line не сработала
     *
     */
    public function listen()
    {
        while ($str = $this->read()) {
            $this->answer($str);
        }

        return $this;
    }

    /**
     * Отвечать на входящий вызов
     *
     * @param $str
     * @return self
     */
    protected function answer($str)
    {
        $request = Request::create()->parse($str);

        if ($request->valid()) {
            try {
                $res = $this->call($request->getName(), $request->getParams());
                $response = Response::create()->prepare($request->getName(), $res);
            } catch (ExecuteException $e) {
                $response = Error::create()->prepare($request->getName(), $e->getMessage());
            }

            $this->write($response->getRaw());
        }

        return $this;
    }

    /**
     * Регистрировать функцию-обработчик входящего вызова
     *
     * @param string $name Имя входящего вызова
     * @param callable $callback Функция-обработчик входящего вызова
     * @return self
     */
    public function addListener($name, callable $callback)
    {
        $this->listeners[$name] = $callback;

        return $this;
    }

    /**
     * Обработать входящий вызов
     *
     * @param string $name Имя входящего вызова
     * @param array $params Параметры входящего вызова
     * @return mixed Результат работы функции-обработчика
     *
     * @throws ExecuteException Когда не задан обработчик для вызов с таким именем
     */
    protected function call($name, $params)
    {
        if (!isset($this->listeners[$name])) {
            throw new ExecuteException("Method '$name' not found");
        }

        return call_user_func_array($this->listeners[$name], (array)$params);
    }

    /**
     * Выполнить исходящий вызов
     *
     * @param string $name Имя исходящего вызова
     * @param array $params Параметры исходящего вызова
     * @return mixed Результат исходящего вызова
     *
     * @throws ExecuteException Когда
     */
    public function execute($name, $params = [])
    {
        $res = null;

        $str = Request::create()
            ->prepare($name, $params)
            ->getRaw();

        $this->write($str);

        $discovered = false;
        $iterations = ceil(self::RESPONSE_TIMEOUT / self::RESPONSE_DELAY);

        while ($iterations-- && !$discovered) {
            $str = $this->read(self::RESPONSE_DELAY);
            if ($str) {
                // что-то получили, может это ответ?
                $response = Response::create()->parse($str);
                if ($response->valid()) {
                    $res = $response->getParams();
                    $discovered = true;
                } else {
                    // если не ответ - тогда это встречный запрос - ответим
                    $this->answer($str);
                }

            }
        }

        if (!$discovered && !feof($this->input)) {
            throw new ExecuteException('No response from input stream');
        }

        return $res;
    }

}