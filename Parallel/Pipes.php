<?php
namespace Parallel;
use Parallel\Exception\ExecuteException;
use Parallel\Exception\RuntimeException;

/**
 * Класс Pipes обслуживает каналы ввода/вывода параллельного процесса, создаваемого классом Worker
 * @package Parallel
 */
class Pipes implements PipesInterface
{
    const STDIN = 0;
    const STDOUT = 1;
    const STDERR = 2;

    /** @var  TransceiverInterface */
    protected $transceiver;
    /** @var  array|null */
    protected $pipes;
    protected $blocked = true;

    /**
     * Конструктор
     *
     * @param TransceiverInterface $transceiver Объект приема/передачи сообщений из потоков ввода/вывода
     */
    public function __construct(TransceiverInterface $transceiver)
    {
        $this->transceiver = $transceiver;
    }

    /**
     * Возвращает новый объект Transceiver
     *
     * @param TransceiverInterface $transceiver Объект приема/передачи сообщений из потоков ввода/вывода
     * @return Pipes
     */
    public static function create(TransceiverInterface $transceiver)
    {
        return new static($transceiver);
    }

    /**
     * Возвращает массив описания дескрипторов для функции proc_open
     *
     * @return array
     */
    public function getDescriptors()
    {
        return [
            self::STDIN => ['pipe', 'r'],  // stdin - канал, из которого дочерний процесс будет читать
            self::STDOUT => ['pipe', 'w'],  // stdout - канал, в который дочерний процесс будет записывать
            self::STDERR => ['pipe', 'w'],  // stderr - канал, в который дочерний процесс будет сообщать об ошибках
        ];
    }

    /**
     * Закрывает потоки вввода/вывода
     */
    public function close()
    {
        if(is_array($this->pipes)) {
            foreach ($this->pipes as $pipe) {
                fclose($pipe);
            }

            $this->pipes = null;
        }
    }

    /**
     * Устанавливает потоки ввода/вывода, которые вернула функция proc_open
     *
     * @param array $pipes Массив потоков ввода/вывода
     *
     * @throws RuntimeException Когда один из потоков в массиве $pipes не является потоком или открыт не в том режиме
     */
    public function set(array $pipes)
    {
        $this->pipes = $pipes;

        $this->transceiver->setStreams($this->pipes[self::STDIN], $this->pipes[self::STDOUT]);

        if ($this->blocked) {
            foreach ($this->pipes as $pipe) {
                stream_set_blocking($pipe, 0);
            }

            $this->blocked = false;
        }

    }

    /**
     * Слушает поток ввода, отвечает на вызовы
     *
     * @throws RuntimeException Когда не удалось прослушать поток ввода
     * @throws RuntimeException Когда попытка прочитать из потока ввода вызвала ошибку
     */
    public function listen()
    {
        $this->transceiver->listen();
    }

    /**
     * Вызвать удаленную функцию-обработчик параллельного процесса с именем вызова $name и с параметрами вызова $params
     *
     * @param string $name Имя вызова
     * @param array $params Параметры выззова
     * @return mixed Ответ функции рабочего процесса
     *
     * @throws ExecuteException Когда параллельный процесс не отвечает
     */
    public function execute($name, $params)
    {
        return $this->transceiver->execute($name, $params);
    }

    /**
     * Зарегистрировать функцию-обработчик вызова из параллельного процесса
     *
     * @param string $name Имя вызова
     * @param callable $callback Функция-обработчик вызова
     * @return self
     */
    public function addListener($name, callable $callback)
    {
        $this->transceiver->addListener($name, $callback);
    }
}