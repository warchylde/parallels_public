<?php
/**
 * Created by PhpStorm.
 * User: warchylde
 * Date: 10.01.16
 * Time: 11:59
 */

namespace Parallel;


interface TransceiverInterface
{
    /**
     * Настраивает потоки ввода/вывода
     *
     * @param resource|string $output Поток вывода, открытый в режиме записи
     * @param resource|string $input Поток ввода, открытый в режиме чтения
     * @return self
     */
    public function setStreams($output, $input);

    /**
     * Слушать поток ввода на наличие входящего вызова и отвечать на него
     *
     * @return self
     */
    public function listen();


    /**
     * Выполнить исходящий вызов
     *
     * @param string $name Имя исходящего вызова
     * @param array $params Параметры исходящего вызова
     * @return mixed
     */
    public function execute($name, $params = []);

    /**
     * Регистрировать функцию-обработчик входящего вызова
     *
     * @param string $name Имя входящего вызова
     * @param callable $callback Функция-обработчик входящего вызова
     * @return self
     */
    public function addListener($name, callable $callback);
}