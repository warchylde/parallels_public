<?php
/**
 * Created by PhpStorm.
 * User: warchylde
 * Date: 08.01.16
 * Time: 17:17
 */

namespace Parallel\Proto;

/**
 * Класс Error реализует сообщение об ошибке межпроцессного взаимодействия
 *
 * @package Parallel
 */

class Error extends Message
{
    /**
     * @inheritdoc
     */
    public function prepare($name, $data, $type = self::TYPE_ERROR)
    {
        return parent::prepare($name, $data, $type);
    }

    /**
     * @inheritdoc
     */
    public function parse($raw, $type = self::TYPE_ERROR)
    {
        return parent::parse($raw, $type);
    }
}