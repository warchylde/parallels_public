<?php
/**
 * Created by PhpStorm.
 * User: warchylde
 * Date: 08.01.16
 * Time: 16:46
 */

namespace Parallel\Proto;

/**
 * Класс Request реализует входящее сообщение межпроцессного взаимодействия
 *
 * @package Parallel
 */
class Request extends Message
{
    /**
     * @inheritdoc
     */
    public function parse($raw, $type = self::TYPE_REQUEST)
    {
        return parent::parse($raw, $type);
    }

    /**
     * @inheritdoc
     */
    public function prepare($name, $data, $type = self::TYPE_REQUEST)
    {
        return parent::prepare($name, $data, $type);
    }

}