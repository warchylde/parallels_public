<?php
/**
 * Created by PhpStorm.
 * User: warchylde
 * Date: 08.01.16
 * Time: 17:17
 */

namespace Parallel\Proto;


/**
 * Класс Response реализует исходящее сообщение межпроцессного взаимодействия
 *
 * @package Parallel
 */
class Response extends Message
{
    /**
     * @inheritdoc
     */
    public function prepare($name, $data, $type = self::TYPE_RESPONSE)
    {
        return parent::prepare($name, $data, $type);
    }

    /**
     * @inheritdoc
     */
    public function parse($raw, $types = [self::TYPE_RESPONSE, self::TYPE_ERROR])
    {
        return parent::parse($raw, $types);
    }

}