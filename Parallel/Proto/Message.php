<?php
namespace Parallel\Proto;

use Parallel\Exception\ExecuteException;

/**
 * Класс Message реализует сообщение межпроцессного взаимодействия
 *
 * @package Parallel
 */
class Message
{
    const FIELD_TYPE = 'type';
    const FIELD_NAME = 'name';
    const FIELD_PARAMS = 'params';

    const TYPE_REQUEST = 'request';
    const TYPE_RESPONSE = 'response';
    const TYPE_ERROR = 'error';

    protected $raw;
    protected $data;
    protected $valid = false;

    /**
     * Возвратить новый объект класса Message
     *
     * @return Message
     */
    public static function create()
    {
        return new static();
    }

    /**
     * Установить значение свойства валидности сообщения
     *
     * @param bool $valid Значение валидности
     * @return self
     */
    protected function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Возвратить значение свойства валидности сообщения
     *
     * @return bool
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Возвратить значение свойства валидности сообщения (синтаксический сахар)
     *
     * @return bool
     */
    public function valid()
    {
        return $this->getValid();
    }

    /**
     * Сохранить строку сообщения
     *
     * @param string $raw Строка сообщения
     * @return self
     */
    protected function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Вернуть строку сообщения
     *
     * @return string
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Сохранить сообщение
     *
     * @param mixed $data
     * @return self
     */
    protected function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Декодировать строку сообщения
     * @param string $raw Строка сообщения
     * @return array Сообщение
     */
    public function decode($raw)
    {
        return @json_decode($raw, true);
    }

    /**
     * Закодировать сообщение
     *
     * @param array $data Сообщение
     * @return string Строка сообщения
     */
    public function encode($data)
    {
        return json_encode($data);
    }

    /**
     * Валидировать сообщение
     *
     * @param array $data Сообщение
     * @param array $types Ожидаемые типы сообщения
     * @return bool Возвращает true если формат сообщения корректный, ожидаемый тип совпадает
     */
    protected function validate($data, $types = [self::TYPE_REQUEST, self::TYPE_RESPONSE, self::TYPE_ERROR])
    {
        return is_array($data)
        && isset($data[self::FIELD_TYPE])
        && isset($data[self::FIELD_NAME])
        && isset($data[self::FIELD_PARAMS])
        && (
        is_array($types)
            ? in_array($data[self::FIELD_TYPE], $types, true)
            : $data[self::FIELD_TYPE] === $types
        );
    }

    /**
     * Вернуть тип сообащения
     *
     * @return string
     */
    public function getType()
    {
        return $this->data[self::FIELD_TYPE];
    }

    /**
     * Вернуть имя сообщения
     *
     * @return string
     */
    public function getName()
    {
        return $this->data[self::FIELD_NAME];
    }

    /**
     * Вернуть параметры сообщения
     *
     * @return mixed
     */
    public function getParams()
    {
        return $this->data[self::FIELD_PARAMS];
    }

    /**
     * Получить сообщение:
     * 1) декодировать
     * 2) провалидировать
     * 3) сохранить
     *
     * @param string $raw Строка сообащения
     * @param array $types Ожидаемые типы сообщения
     * @return self
     *
     * @throws ExecuteException Когда получено сообщение об ошибке
     */
    public function parse($raw, $types = [self::TYPE_REQUEST, self::TYPE_RESPONSE, self::TYPE_ERROR])
    {
        $this->setRaw($raw);
        $data = $this->decode($raw);
        $validity = $this->validate($data, $types);
        $this->setValid($validity);

        if ($validity) {
            if ($data[self::FIELD_TYPE] === self::TYPE_ERROR) {
                throw new ExecuteException($data[self::FIELD_PARAMS]);
            }

            $this->setData($data);
        }

        return $this;
    }

    /**
     * Сформировать строку сообщения
     *
     * @param string $name Имя сообщения
     * @param array $data Данные сообещния
     * @param string $type
     * @return self
     */
    public function prepare($name, $data, $type)
    {
        $data = [
            self::FIELD_TYPE => $type,
            self::FIELD_NAME => $name,
            self::FIELD_PARAMS => $data
        ];

        $this->setRaw($this->encode($data));

        return $this;
    }
}

