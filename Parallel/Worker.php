<?php
namespace Parallel;

use Parallel\Exception\LogicException;
use Parallel\Exception\RuntimeException;

/**
 * Worker - класс для управления параллельным процессом (посредством семейства proc_ функций).
 *
 * @package Parallel
 * @api
 */
class Worker implements WorkerInterface
{
    const STATUS_READY = 'ready';
    const STATUS_STARTED = 'started';
    const STATUS_TERMINATED = 'terminated';

    /** @var Pipes */
    protected $processPipes;
    /** @var string */
    protected $command;
    /** @var  resource */
    protected $process;

    protected $processInformation;
    protected $status = self::STATUS_READY;

    /**
     * Конструктор
     *
     * @param PipesInterface $pipes Объект для обслуживания каналов ввода/вывода параллельного процесса
     */
    public function __construct(PipesInterface $pipes)
    {
        $this->processPipes = $pipes;
    }

    /**
     * Возвращает новый объект Worker
     *
     * @param PipesInterface $pipes Объект для обслуживания каналов ввода/вывода параллельного процесса
     * @return Worker
     */
    public static function create(PipesInterface $pipes)
    {
        return new static($pipes);
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * Установить команду для выполнения
     *
     * @param string $command Команда для выполнения
     * @return self
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Получить команду для выполнения
     * @return string $command Команда для выполнения
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Запустить рабочий процесс
     *
     * @return self
     *
     * @throws RuntimeException Когда процесс не может быть запущен
     * @throws RuntimeException Когда процесс уже запущен
     * @throws LogicException   Когда процесс не сконфигурирован
     *
     */
    public function start()
    {
        $command = $this->getCommand();

        if (!$command) {
            throw new LogicException('Worker is not configured');
        }

        if ($this->isRunning()) {
            throw new RuntimeException('Worker is already running');
        }

        $this->reset();

        $this->process = proc_open($command, $this->processPipes->getDescriptors(), $pipes);

        if (!is_resource($this->process)) {
            throw new RuntimeException('Unable to launch a new process.');
        }

        $this->processPipes->set($pipes);

        $this->status = self::STATUS_STARTED;

        return $this;
    }

    /**
     * Обновляет состояние процесса и возвращает его статус
     *
     * @return bool true если процесс запущен, иначе false
     */
    public function isRunning()
    {
        if (self::STATUS_STARTED !== $this->status) {
            return false;
        }

        $this->updateStatus();

        return $this->processInformation['running'];
    }

    /**
     * Сбросить состояние рабочего процесса на изначальное
     */
    protected function reset()
    {
        $this->command = null;
        $this->process = null;
        $this->status = self::STATUS_READY;
    }

    /**
     * Обновить статус рабочего процесса, обслужить каналы ввода/вывода процесса
     */
    protected function updateStatus()
    {
        if (self::STATUS_STARTED !== $this->status) {
            return;
        }

        $this->processInformation = proc_get_status($this->process);

        $this->processPipes->listen();

        if (!$this->processInformation['running']) {
            $this->close();
        }
    }

    /**
     * Закрыть рабочий процесс
     */
    protected function close()
    {
        $this->processPipes->close();

        if (is_resource($this->process)) {
            proc_close($this->process);
        }

        $this->status = self::STATUS_TERMINATED;
    }

    /**
     * Вызвать удаленную функцию-обработчик параллельного процесса с именем вызова $name и с параметрами вызова $params
     *
     * @param string $name Имя вызова
     * @param array $params Параметры выззова
     * @return mixed Ответ функции рабочего процесса
     */
    public function __call($name, $params)
    {
        return $this->processPipes->execute($name, $params);
    }

    /**
     * Зарегистрировать функцию-обработчик вызова из рабочего процесса
     *
     * @param string $name Имя вызова
     * @param callable $callback Функция-обработчик вызова
     * @return self
     */
    public function addListener($name, callable $callback)
    {
        $this->processPipes->addListener($name, $callback);

        return $this;
    }

    /**
     * Настраивать команду для выполнения.
     *
     * Метод формирует php-код и передает его на выполнение запускаемому экземпляру php через ключ php -r '<code>'
     *
     * Пример:
     * $worker->prepare('pi.conf', 'pi.calculator.service', ['setOptions' => [['iterations' => 8437452]], 'run' => null]);
     *
     * Сформирует код:
     * require 'autoload.php';
     * $service = \Container\Container::create('pi.conf')->get('pi.calculator.service');
     * $service->setOptions(json_decode('{"iterations":8437452}', true));
     * $service->run();
     *
     * И настроит запуск php-cli:
     * /usr/bin/php5 -r 'require '\''autoload.php'\''; $service = \Container\Container::create('\''pi.conf'\'')->get('\''pi.calculator.service'\''); $service->setOptions(json_decode('\''{"iterations":6992781}'\'', true)); $service->run();'
     *
     * @param string $config Путь до конфигурации контейнера
     * @param string $service Название сервиса
     * @param array $methods Массив методов с параметрами вызова, которые будут выполнены для сервиса, может быть пустой
     * @return self
     */
    public function prepare($config, $service, array $calls = null)
    {
        $calls = $this->serializeCalls($calls);
        $script = 'require \'autoload.php\'; $service = \\Container\\Container::create(\''.$config.'\')->get(\''.$service.'\'); '.$calls;

        return $this->setCommand(PHP_BINARY.' -r '.escapeshellarg($script));
    }

    protected function serializeCalls($calls)
    {
        return join(' ', array_map(function($method, $params) {
            return "\$service->{$method}({$this->serializeParams($params)});";
        }, array_keys($calls), array_values($calls)));
    }

    protected function serializeParams($params)
    {
        return join(', ', array_map(function($param) {
            return 'json_decode(\''.json_encode($param).'\', true)';
        }, (array) $params));
    }
}