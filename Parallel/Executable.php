<?php
namespace Parallel;
use Parallel\Exception\RuntimeException;


/**
 * Абстрактный класс Executable реализует распаралеливаемый процесс.
 *
 * При создании в родителе ему можно передать настройки и получить объект класса Worker
 * для запуска в параллельном процессе себя самого.
 *
 * При запуске в параллельном потоке восстанавливает переданные настройки, подключает потоки ввода/вывода, запускается
 * главная рабочая функция run.
 * В ней вызывается функцию prepare и запускается цикл обработки входящих сообщений.
 * В каждой итерации цикла обработки вызывается функция iterate.
 * Если функция iterate вернет false - цикл закончится, работа класса будет завершена.
 *
 * @package Parallel
 */
abstract class Executable
{
    /** @var TransceiverInterface  */
    protected $transceiver;
    /** @var Worker */
    protected $worker;
    /** @var  string */
    protected $name;
    /** @var  string */
    protected $config;


    protected $options = [];

    /**
     * Функция подготовки цикла обработки
     */
    abstract protected function prepare();

    /**
     * Функция итерации цикла обработки
     *
     * @param int $iteration Номер итерации
     * @return mixed Если будет возвращено значение false - цикл обработки завершит свою работу
     */
    abstract protected function iterate($iteration);

    /**
     * Конструтор
     *
     * @param TransceiverInterface $transceiver Приемопередатчик сообщений через потоки ввода/вывода
     * @param WorkerInterface $worker Экземпляр рабочего процесса
     * @param string $name Название сервиса в контейнере
     * @param string $config Путь до конфигурационного файла
     */
    public function __construct(TransceiverInterface $transceiver, WorkerInterface $worker, $name, $config)
    {
        $this->transceiver = $transceiver;
        $this->worker = $worker;
        $this->name = $name;
        $this->config = $config;
    }

    /**
     * Возвратить новый объект класса наследника Executable
     *
     * @param TransceiverInterface $transceiver Приемопередатчик сообщений через потоки ввода/вывода
     * @param WorkerInterface $worker Экземпляр рабочего процесса
     * @param string $name Название сервиса в контейнере
     * @param string $config Путь до конфигурационного файла
     * @return self
     */
    public static function create(TransceiverInterface $transceiver, WorkerInterface $worker, $name, $config)
    {
        return new static($transceiver, $worker, $name, $config);
    }

    /**
     * Настроить потоки ввода/вывода
     *
     * @param resource|string $output Поток вывода, открытый в режиме записи
     * @param resource|string $input Поток ввода, открытый в режиме чтения
     * @return self
     *
     * @throws RuntimeException Когда поток не поток или он открыт в несоответствующем режиме
     */
    public function setStreams($output = 'php://stdout', $input = 'php://stdin')
    {
        $this->transceiver->setStreams($output, $input);

        return $this;
    }

    /**
     * Зарегистрировать функцию-обработчик входящего вызова
     *
     * @param string $name Имя входящего вызова
     * @param callable $callback Функция-обработчик входящего вызова
     * @return self
     */
    public function addListener($name, $callback)
    {
        $this->transceiver->addListener($name, $callback);

        return $this;
    }

    /**
     * Запустить цикл обработки входящих сообщений
     *
     * @return self
     */
    public function run()
    {
        $i = 0;

        $this->setStreams();
        $this->prepare();

        while(false !== $this->iterate($i++)) {
            $this->transceiver->listen();
        }

        return $this;
    }

    /**
     * Установить значение опции
     *
     * @param string $name Имя опции
     * @param mixed $value Значение опции
     * @return self
     */
    public function setOption($name, $value)
    {
        $this->options[$name] = $value;

        return $this;
    }

    /**
     * Установить опции
     * Опции могут быть переданы массивом или сериализованной строкой (json), которая будет декодирована
     *
     * @param array|string $options Массив опций или строка с сериализованными опциями
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = is_array($options) ? $options : json_decode($options, true);

        return $this;
    }

    /**
     * Вернуть значние опции
     *
     * @param string $name Имя опции
     * @param mixed $default Дефолтовое значние опции, используемое при отсутствии назначенного
     * @return mixed
     */
    public function getOption($name, $default = null)
    {
        return array_key_exists($name, $this->options) ? $this->options[$name] : $default;
    }

    /**
     * Возвратить экземпляр рабочего процесса Worker для запуска
     *
     * @return Worker
     */
    public function getWorker()
    {
        return $this->worker->prepare($this->config, $this->name, ['setOptions' => [$this->options], 'run' => null]);
    }
}