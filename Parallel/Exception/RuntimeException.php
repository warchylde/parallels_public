<?php
namespace Parallel\Exception;

/**
 * Класс RuntimeException для ошибок выполнения
 *
 * @package Parallel
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface
{

}