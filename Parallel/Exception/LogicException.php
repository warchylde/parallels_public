<?php
namespace Parallel\Exception;

/**
 * Исключение класса LogicException случаются при логических ошибках программиста
 *
 * @package Parallel
 */
class LogicException extends \LogicException implements ExceptionInterface
{
}
