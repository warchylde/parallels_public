<?php
namespace Parallel\Exception;

/**
 * Исключения класса ExecuteException случаются при ошибках межпроцессного вззаимодействия
 *
 * @package Parallel\Exception
 */
class ExecuteException extends \ErrorException implements ExceptionInterface
{

}