<?php
namespace Parallel;

interface PipesInterface
{
    /**
     * Возвращает массив описания дескрипторов для функции proc_open
     *
     * @return array
     */
    public function getDescriptors();

    /**
     * Устанавливает потоки ввода/вывода, которые вернула функция proc_open
     *
     * @param array $pipes Массив потоков ввода/вывода
     */
    public function set(array $pipes);

    /**
     * Закрывает потоки вввода/вывода
     */
    public function close();

    /**
     * Зарегистрировать функцию-обработчик вызова из параллельного процесса
     *
     * @param string $name Имя вызова
     * @param callable $callback Функция-обработчик вызова
     * @return self
     */
    public function addListener($name, callable $callback);

    /**
     * Слушает поток ввода, отвечает на вызовы
     */
    public function listen();

    /**
     * Вызвать удаленную функцию-обработчик параллельного процесса с именем вызова $name и с параметрами вызова $params
     *
     * @param string $name Имя вызова
     * @param array $params Параметры выззова
     * @return mixed Ответ функции рабочего процесса
     */
    public function execute($name, $params);
}