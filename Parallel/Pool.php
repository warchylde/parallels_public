<?php
namespace Parallel;
use Parallel\Exception\LogicException;
use Parallel\Exception\RuntimeException;

/**
 * Класс Pool управляет пулом рабочих
 * @package Parallel
 * @api
 */
class Pool
{
    const SLEEP_DELAY = 3e5; // usecs

    /** @var WorkerInterface[] */
    protected $workers = [];

    /**
     * Добавить рабочий процесс в пул
     * @param WorkerInterface $worker
     * @return self
     */
    public function insert(WorkerInterface $worker)
    {
        $this->workers[] = $worker;

        return $this;
    }

    /**
     * Запустить пул рабочих процессов и ждать окончания их работы
     *
     * @param callable|null $callback Колбек-функция запускает при каждого итерации опроса очередного рабочего процесса
     * @return self
     *
     * @throws RuntimeException Когда один из рабочих процессов не может быть запущен
     * @throws RuntimeException Когда один из рабочих процессов уже запущен
     * @throws LogicException   Когда один из рабочих процессов не сконфигурен
     */
    public function run($callback = null)
    {
        return $this
            ->start()
            ->wait($callback)
        ;
    }

    /**
     * Запустить пул рабочих процессов
     *
     * @throws RuntimeException Когда один из рабочих процессов не может быть запущен
     * @throws RuntimeException Когда один из рабочих процессов уже запущен
     * @throws LogicException   Когда один из рабочих процессов не сконфигурен
     *
     * @return self
     */
    public function start()
    {
        foreach ($this->workers as $worker) {
            $worker->start();
        }

        return $this;
    }

    /**
     * Ждать окончания работы всех запущеных процессов в пуле
     *
     * @param callable|null $callback Колбек-функция запускает при каждого итерации опроса очередного рабочего процесса
     * @return self
     */
    public function wait($callback = null)
    {
        $callback = is_callable($callback) ? $callback : null;
        $num = count($this->workers);

        while ($num) {
            $num = 0;

            foreach ($this->workers as $index => $worker) {
                if($worker->isRunning()) {
                    $num++;

                    if($callback) {
                        call_user_func($callback, $worker, $index);
                    }
                }
            }

            usleep(self::SLEEP_DELAY);
        }

        return $this;
    }

    /**
     * Вернуть пул (массив рабочих процессов)
     *
     * @return WorkerInterface[]
     */
    public function getWorkers()
    {
        return $this->workers;
    }
}