<?php
/**
 * Created by PhpStorm.
 * User: warchylde
 * Date: 07.01.16
 * Time: 18:41
 */

namespace Parallel;

interface WorkerInterface
{
    /**
     * Запустить рабочий процесс
     *
     * @return self
     */
    public function start();

    /**
     * Возвращает статус рабочего процесса
     *
     * @return bool true если процесс запущен, иначе false
     */
    public function isRunning();
}