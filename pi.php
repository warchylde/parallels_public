<?php
require 'autoload.php';

use Container\Container;
use Pi\Pi;

$start = time();

$pi = Container::create('pi.conf')
    ->get('pi.service')
    ->calculate(function(Pi $pi) use($start) {
        $value = $pi->get();
        $secs = time() - $start;

        echo "Seconds from the start: $secs, Pi: $value\n";
    })
;
